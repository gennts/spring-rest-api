package me.genti.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import me.genti.model.Provimi;

public class ProvimiRM implements RowMapper<Provimi> {
	@Override
	public Provimi mapRow(ResultSet rs, int rowNum) throws SQLException {
		Provimi p = new Provimi();
		p.setVleresimi(rs.getInt("vleresimi"));
		return p;
	}
}
