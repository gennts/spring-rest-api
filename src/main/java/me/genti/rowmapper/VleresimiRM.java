package me.genti.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import me.genti.model.Vleresimi;

public class VleresimiRM implements RowMapper<Vleresimi>{
	@Override
	public Vleresimi mapRow(ResultSet rs, int rowNum) throws SQLException {
		Vleresimi v = new Vleresimi();
		v.setVleresimi(rs.getInt("vleresimi"));
		return v;
	}
}
