package me.genti.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import me.genti.model.ProjektiGrupor;

public class ProjektiRM implements RowMapper<ProjektiGrupor>{
	@Override
	public ProjektiGrupor mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProjektiGrupor pg = new ProjektiGrupor();
		pg.setVleresimi(rs.getInt("vleresimi"));
		return pg;
	}
}
