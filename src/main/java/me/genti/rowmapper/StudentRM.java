package me.genti.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import me.genti.model.Student;
/*
 * private Integer id;
	private String emri;
	private String mbiemri;
	private String email;
	private String nrtel;
 */
public class StudentRM implements RowMapper<Student>{
	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
		Student s = new Student();
		s.setId(rs.getInt("id"));
		s.setEmri(rs.getString("emri"));
		s.setMbiemri(rs.getString("mbiemri"));
		s.setEmail(rs.getString("email"));
		s.setNrtel(rs.getString("nrtel"));
		return s;
	}
}
