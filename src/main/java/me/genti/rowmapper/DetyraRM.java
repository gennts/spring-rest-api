package me.genti.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import me.genti.model.Detyra;

public class DetyraRM implements RowMapper<Detyra>{

	@Override
	public Detyra mapRow(ResultSet rs, int rowNum) throws SQLException {
		Detyra d = new Detyra();
		d.setId(rs.getInt("id"));
		d.setId_lenda(rs.getInt("id_lenda"));
		d.setTitulli(rs.getString("titulli"));
		return d;
	}

}
