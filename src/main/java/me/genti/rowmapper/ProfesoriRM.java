
package me.genti.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import me.genti.model.Professor;
/*
 * private Integer id;
	private String emri;
	private String mbiemri;
	private String email;
	private String nrtel;
 */
public class ProfesoriRM implements RowMapper<Professor>{
	@Override
	public Professor mapRow(ResultSet rs, int rowNum) throws SQLException {
		Professor p = new Professor();
		p.setId(rs.getInt("id"));
		p.setEmri(rs.getString("emri"));
		p.setMbiemri(rs.getString("mbiemri"));
		p.setEmail(rs.getString("email"));
		p.setNrtel(rs.getString("nrtel"));
		return p;
	}
}
