package me.genti.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import me.genti.model.Pjesmarrja;

public class PjesmarrjaRM implements RowMapper<Pjesmarrja> {
	@Override
	public Pjesmarrja mapRow(ResultSet rs, int rowNum) throws SQLException {
		Pjesmarrja p = new Pjesmarrja();
		p.setPjesmarrja(rs.getInt("pjesmarrja"));
		return p;
	}
}
