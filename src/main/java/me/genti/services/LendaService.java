package me.genti.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.genti.dao.BaseDAO;
import me.genti.dao.interfaces.DetyraDAOInterface;
import me.genti.dao.interfaces.LendaDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Detyra;
import me.genti.model.Lenda;
import me.genti.services.interfaces.LendaServiceInterface;

@Service
public class LendaService extends BaseDAO implements LendaServiceInterface {
	
	@Autowired
	private LendaDAOInterface lendaDAO;
	@Autowired
	private DetyraDAOInterface detyraDAO;
	
	@Override
	public boolean insert(Lenda lenda) throws EntityExistException {
		return lendaDAO.insert(lenda);
	}
	
	@Override
	public boolean update(Lenda lenda) throws EntityExistException {
		return lendaDAO.update(lenda);
	}
	
	@Override
	public boolean delete(Lenda lenda) {
		return lendaDAO.delete(lenda);
	}
	
	@Override
	public boolean insert(Detyra detyra) throws EntityExistException {
		return detyraDAO.insert(detyra);
	}
	
	@Override
	public boolean update(Detyra detyra) throws EntityExistException {
		return detyraDAO.update(detyra);
	}
	
	@Override
	public boolean delete(Detyra detyra) {
		return detyraDAO.delete(detyra);
	}
	@Override
	public List<Detyra> getDetyrat(Integer lendaID) {
		return detyraDAO.getDetyrat(lendaID);
	}
	
	

}
