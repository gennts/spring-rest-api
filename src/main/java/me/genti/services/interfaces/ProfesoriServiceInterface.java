package me.genti.services.interfaces;

import java.util.List;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Detyra;
import me.genti.model.Lenda;
import me.genti.model.Pjesmarrja;
import me.genti.model.Professor;
import me.genti.model.ProjektiGrupor;
import me.genti.model.Provimi;
import me.genti.model.Student;
import me.genti.model.Vleresimi;

public interface ProfesoriServiceInterface {
	
	/**
	 * Persist a new {@link Professor} object in persistent data.
	 * @param profesor object to be persisted.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Professor profesor)throws EntityExistException;
	
	/**
	 * Merge {@code this} {@link Professor} object with its {@code Entity} in persistent data.
	 * @param profesor object to be merged.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(Professor profesor)throws EntityExistException;
	
	/**
	 * Remove {@link Professor} from persistent data.
	 * @param profesor object to be removed.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Professor profesor);
	
	/**
	 * Retrieve all {@link Professor}s found in persistent data.
	 * @return {@code List} of all {@link Professor}
	 */
	public List<Professor> getAllProfessors();
	
	/**
	 * Persist a new {@link Pjesmarrja} into persistent data.
	 * @param pjesmarrja object to be persisted
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Pjesmarrja pjesmarrja) throws EntityExistException;
	
	/**
	 * Remove a {@link Pjesmarrja} from persisted data.
	 * @param pjesmarrja object to be removed
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Pjesmarrja pjesmarrja);
	
	/**
	 * Method retrieves student attendance at a specified {@link Lenda} given by a {@link Professor}
	 * @param studentID {@link Student#getId()}
	 * @param professorID {@link Professor#getId()}
	 * @param lendaID {@link Lenda#getId()}
	 * @return {@link Integer} representing {@link Student} attendance for specified {@link Lenda} and {@link Professor} if
	 * 			result is found, otherwise return {@code null}
	 */
	public Integer getStudentAttendance(Integer studentID, Integer professorID, Integer lendaID);
	
	/**
	 * Persist a new {@link ProjektiGrupor} to persistent data.
	 * @param pg object to be persisted.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(ProjektiGrupor pg)throws EntityExistException;
	
	/**
	 * Merge {@code this} {@link ProjektiGrupor} object with its persisted {@code Entity} data.
	 * @param pg object to be persisted
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(ProjektiGrupor pg)throws EntityExistException;
	
	/**
	 * Remove a {@link ProjektiGrupor} object from persistent data. 
	 * @param pg object to be removed.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(ProjektiGrupor pg);
	
	/**
	 * Retrieve the {@link ProjektiGrupor} evaluation of a {@link Student} in a specified {@link Lenda}
	 * @param studentID {@link Student#getId()}
	 * @param lendaID {@link Lenda#getId()}
	 * @return {@link Integer} representing {@link Student} evaluation if found, otherwise {@code null}
	 */
	public Integer getVleresimiProjektiGrupor(Integer studentID, Integer lendaID);
	
	/**
	 * Persiste a new {@code Provimi} into persistent data.
	 * @param provimi object to be persisted
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Provimi provimi)throws EntityExistException;
	
	/**
	 * Merge {@code this} {@link Provimi} object changes with its {@code Entity} in persistent data.
	 * @param provimi object with changes to be merged.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(Provimi provimi) throws EntityExistException;
	
	/**
	 * Remove {@link Provimi} from persistent data.
	 * @param provimi object to be removed from persisted data.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Provimi provimi);
	
	/**
	 * Get exam evaluation ({@link Provimi#getVleresimi()}) for a {@link Student} set by a {@link Professor}
	 * @param lendaID {@link Lenda#getId()}
	 * @param studentID {@link Student#getId()}
	 * @return {@link Integer} representing student evaluation for a exam if found, otherwise {@code null}
	 */
	public Integer getProvimiVleresimi(Integer lendaID, Integer studentID);
	
	/**
	 * Persists a new {@link Vleresimi} object into persisten data.
	 * @param vleresimi object to be persisted.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Vleresimi vleresimi) throws EntityExistException;
	
	/**
	 * Merge {@link Vleresimi} with a corresponding persistent Entity in database
	 * @param vleresimi object to be merge
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(Vleresimi vleresimi) throws EntityExistException;
	
	/**
	 * Remove {@code this} object from persistent data.
	 * @param vleresimi object to be removed.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Vleresimi vleresimi);
	
	/**
	 * Retrieve evaluation from persistent data based on keys provided on method paramaters.
	 * @param detyraID {@link Detyra#getId()}
	 * @param studentID {@link Student#getId()}
	 * @return {@link Integer} if result based on keys received is found, otherwise {@code null}
	 */
	public Integer getVleresimiDetyra(Integer detyraID, Integer studentID);
	
	/**
	 * Performs marks calculation based on each and every data available for a {@link Student}
	 * @param studentID {@link Student#getId()}
	 * @param profesorID {@link Professor#getId()}
	 * @param lendaID {@link Lenda#getId()}
	 * @return Integer representation of a Student mark if everything on calculation goes OK otherwise {@code null}
	 */
	public Integer kalkuloNoten(Integer studentID, Integer profesorID, Integer lendaID);
}
