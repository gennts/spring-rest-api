package me.genti.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.genti.dao.BaseDAO;
import me.genti.dao.interfaces.StudentiDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Student;
import me.genti.services.interfaces.StudentiServiceInterface;

@Service
public class StudentiService extends BaseDAO implements StudentiServiceInterface {
	@Autowired
	private StudentiDAOInterface studentiDAO;

	@Override
	public boolean insert(Student student) throws EntityExistException {
		return studentiDAO.insert(student);
	}

	@Override
	public boolean update(Student student) throws EntityExistException {
		return studentiDAO.update(student);
	}

	@Override
	public boolean delete(Student student) {
		return studentiDAO.delete(student);
	}

	@Override
	public List<Student> getAllStudents() {
		return studentiDAO.getAllStudents();
	}

	

}
