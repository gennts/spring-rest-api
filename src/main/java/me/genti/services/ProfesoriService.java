package me.genti.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.genti.dao.BaseDAO;
import me.genti.dao.interfaces.DetyraDAOInterface;
import me.genti.dao.interfaces.DetyraVleresimiDAOInterface;
import me.genti.dao.interfaces.PjesmarrjaDAOInterface;
import me.genti.dao.interfaces.ProfesoriDAOInterface;
import me.genti.dao.interfaces.ProjektiGruporDAOInterface;
import me.genti.dao.interfaces.ProvimiDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Pjesmarrja;
import me.genti.model.Professor;
import me.genti.model.ProjektiGrupor;
import me.genti.model.Provimi;
import me.genti.model.Vleresimi;
import me.genti.services.interfaces.ProfesoriServiceInterface;

@Service
public class ProfesoriService extends BaseDAO implements ProfesoriServiceInterface {
	
	@Autowired
	private ProfesoriDAOInterface profesoriDAO;
	@Autowired
	private PjesmarrjaDAOInterface pjesmarrjaDAO;
	@Autowired
	private ProjektiGruporDAOInterface projektiDAO;
	@Autowired
	private ProvimiDAOInterface provimiDAO;
	@Autowired 
	private DetyraVleresimiDAOInterface detyraVleresimiDAO;
	@Autowired
	private DetyraDAOInterface detyraDAO;

	@Override
	public boolean insert(Professor profesor) throws EntityExistException {
		return profesoriDAO.insert(profesor);
	}



	@Override
	public boolean update(Professor profesor) throws EntityExistException {
		return profesoriDAO.update(profesor);
	}



	@Override
	public boolean delete(Professor profesor) {
		return profesoriDAO.delete(profesor);
	}



	@Override
	public List<Professor> getAllProfessors() {
		return profesoriDAO.getAllProfessors();
	}



	@Override
	public boolean insert(Pjesmarrja pjesmarrja) throws EntityExistException {
		return pjesmarrjaDAO.insert(pjesmarrja);
	}



	@Override
	public boolean delete(Pjesmarrja pjesmarrja) {
		return pjesmarrjaDAO.delete(pjesmarrja);
	}



	@Override
	public Integer getStudentAttendance(Integer studentID, Integer professorID, Integer lendaID) {
		return pjesmarrjaDAO.getStudentAttendance(studentID, professorID, lendaID);
	}



	@Override
	public boolean insert(ProjektiGrupor pg) throws EntityExistException {
		return projektiDAO.insert(pg);
	}



	@Override
	public boolean update(ProjektiGrupor pg) throws EntityExistException {
		return projektiDAO.update(pg);
	}



	@Override
	public boolean delete(ProjektiGrupor pg) {
		return projektiDAO.delete(pg);
	}



	@Override
	public Integer getVleresimiProjektiGrupor(Integer studentID, Integer lendaID) {
		return projektiDAO.getVleresimiProjektiGrupor(studentID, lendaID);
	}



	@Override
	public boolean insert(Provimi provimi) throws EntityExistException {
		return provimiDAO.insert(provimi);
	}



	@Override
	public boolean update(Provimi provimi) throws EntityExistException {
		return provimiDAO.update(provimi);
	}



	@Override
	public boolean delete(Provimi provimi) {
		return provimiDAO.delete(provimi);
	}



	@Override
	public Integer getProvimiVleresimi(Integer lendaID, Integer studentID) {
		return provimiDAO.getVleresimi(lendaID, studentID);
	}



	@Override
	public boolean insert(Vleresimi vleresimi) throws EntityExistException {
		return detyraVleresimiDAO.insert(vleresimi);
	} 



	@Override
	public boolean update(Vleresimi vleresimi) throws EntityExistException {
		return detyraVleresimiDAO.update(vleresimi);
	}



	@Override
	public boolean delete(Vleresimi vleresimi) {
		return detyraVleresimiDAO.delete(vleresimi);
	}



	@Override
	public Integer getVleresimiDetyra(Integer detyraID, Integer studentID) {
		return detyraVleresimiDAO.getVleresimiDetyra(detyraID, studentID);
	}
	
	@Transactional
	@Override
	public Integer kalkuloNoten(Integer studentID, Integer profesorID, Integer lendaID) {
		
		List<Integer> vlersimet_detyres = new ArrayList<>();
		detyraDAO.getDetyrat(lendaID).forEach(detyra -> {
			vlersimet_detyres.add(detyraVleresimiDAO.getVleresimiDetyra(detyra.getId(), studentID));
		});
			
		final Double vlersimidetyres = vlersimet_detyres.stream().mapToInt(Integer::intValue).sum() * 0.15;
		
		final Double vlersimiIprojektit = getVleresimiProjektiGrupor(studentID, lendaID) * 0.35;
		
		final Double vlersimiProvimit = getProvimiVleresimi(lendaID, studentID) * 0.4;
		
		final Double pjesmarrja = getStudentAttendance(studentID, profesorID, lendaID) * 0.1;
		
		return (int)((vlersimidetyres+vlersimiIprojektit+vlersimiProvimit+pjesmarrja) / 10);
	}


}
