package me.genti.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.genti.exceptions.EntityExistException;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.DetyraDAOInterface;
import me.genti.model.Detyra;
import me.genti.rowmapper.DetyraRM;

@Repository
public class DetyraDAO extends BaseDAO implements DetyraDAOInterface {
	
	private final String INSERT_QUERY = "INSERT INTO detyra VALUES(:id, :id_lenda, :titulli)";
	private final String GET_QUERY = "SELECT * FROM detyra WHERE id_lenda=?";
	private final String DELETE_QUERY = "DELETE FROM detyra WHERE id=?";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean insert(Detyra detyra)throws EntityExistException {
		@SuppressWarnings("rawtypes")
		Map m = new HashMap();	
		m.put("id", detyra.getId());
		m.put("id_lenda", detyra.getId_lenda());
		m.put("titulli", detyra.getTitulli());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try{
			return getNamedParameterJdbcTemplate().update(INSERT_QUERY, ps) != 0;
		}catch(Throwable t){
			throw new EntityExistException(t.getMessage());
		}
	}

	@Override
	public boolean update(Detyra detyra) {
		throw new UnsupportedOperationException("Function not supported yet!");
	}
	
	@Override
	public List<Detyra> getDetyrat(Integer lendaID) {
		return getJdbcTemplate().query(GET_QUERY, new DetyraRM(), lendaID);
	}

	@Override
	public boolean delete(Detyra detyra) {
		return getJdbcTemplate().update(DELETE_QUERY, detyra.getId()) != 0;
	}
}
