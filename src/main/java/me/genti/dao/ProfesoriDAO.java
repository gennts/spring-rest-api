package me.genti.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.ProfesoriDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Professor;
import me.genti.rowmapper.ProfesoriRM;

@Repository
public class ProfesoriDAO extends BaseDAO implements ProfesoriDAOInterface {
	
	private final String INSERT_QUERY = "INSERT INTO profesori VALUES(:id, :emri, :mbiemri, :email, :nrtel)";
	private final String UPDATE_QUERY = "UPDATE profesori SET emri=:emri, mbiemri=:mbiemri, email=:email, nrtel=:nrtel WHERE id=:id";
	private final String GET_QUERY = "SELECT * FROM profesori";
	private final String DELETE_QUERY = "DELETE FROM profesori WHERE id=?";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean insert(Professor profesor) throws EntityExistException {
		@SuppressWarnings("rawtypes") Map m = new HashMap();
		m.put("id", profesor.getId());
		m.put("emri", profesor.getEmri());
		m.put("mbiemri", profesor.getMbiemri());
		m.put("email", profesor.getEmail());
		m.put("nrtel", profesor.getNrtel());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try {
			return getNamedParameterJdbcTemplate().update(INSERT_QUERY, ps) != 0;
		} catch (Throwable t) {
			throw new EntityExistException("\"error\":{\n\t\"errortxt\": \"A Professor with that ID already exists!\"\n}");
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean update(Professor profesor) throws EntityExistException{
		@SuppressWarnings("rawtypes")
		Map m = new HashMap();
		m.put("id", profesor.getId());
		m.put("emri", profesor.getEmri());
		m.put("mbiemri", profesor.getMbiemri());
		m.put("email", profesor.getEmail());
		m.put("nrtel", profesor.getNrtel());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try {
			return getNamedParameterJdbcTemplate().update(UPDATE_QUERY, ps) != 0;
		} catch (Throwable t) {
			throw new EntityExistException(t.getMessage());
		}
	}

	@Override
	public List<Professor> getAllProfessors() {
		return getJdbcTemplate().query(GET_QUERY, new ProfesoriRM());
	}
	@Override
	public boolean delete(Professor profesor) {
		return getJdbcTemplate().update(DELETE_QUERY, profesor.getId()) != 0;
	}
}
