package me.genti.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.PjesmarrjaDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Pjesmarrja;
import me.genti.rowmapper.PjesmarrjaRM;

@Repository
public class PjesmarrjaDAO extends BaseDAO implements PjesmarrjaDAOInterface{
	
	private final String INSERT_QUERY = "INSERT INTO pjesmarrja VALUES(:sid, :lid, :pid, :pjes)";
	private final String GET_QUERY = "SELECT pjesmarrja FROM pjesmarrja WHERE id_studenti=? AND id_lenda=? AND id_profesori=?";
	private final String DELETE_QUERY = "DELETE FROM pjesmarrja WHERE id_lenda=? AND id_profesori=? AND id_studenti=?";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean insert(Pjesmarrja pjesmarrja) throws EntityExistException{
		@SuppressWarnings("rawtypes")
		Map m = new HashMap();
		m.put("sid", pjesmarrja.getId_studenti());
		m.put("lid", pjesmarrja.getId_lenda());
		m.put("pid", pjesmarrja.getId_profesori());
		m.put("pjes", pjesmarrja.getPjesmarrja());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try{
			return getNamedParameterJdbcTemplate().update(INSERT_QUERY, ps) != 0;
		}catch(Throwable t){
			throw new EntityExistException(t.getMessage());
		}
	}

	@Override
	public Integer getStudentAttendance(Integer studentID, Integer professorID, Integer lendaID) {
		return ((Pjesmarrja)getJdbcTemplate().queryForObject(GET_QUERY, new PjesmarrjaRM(), studentID, lendaID, professorID)).getPjesmarrja();
	}

	@Override
	public boolean update(Pjesmarrja pjesmarrja) throws EntityExistException {
		throw new UnsupportedOperationException("Operation not supported yet!");
	}

	@Override
	public boolean delete(Pjesmarrja pjesmarrja) {
		return getJdbcTemplate().update(DELETE_QUERY, pjesmarrja.getId_lenda(), pjesmarrja.getId_profesori(), pjesmarrja.getId_studenti()) != 0;
	}

}
