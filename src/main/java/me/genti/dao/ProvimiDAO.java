package me.genti.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.ProvimiDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Provimi;
import me.genti.rowmapper.ProvimiRM;

@Repository
public class ProvimiDAO extends BaseDAO implements ProvimiDAOInterface{

	private final String INSERT_QUERY = "INSERT INTO provimi VALUES(:lid, :sid, :vleresimi)";
	private final String GET_QEURY = "SELECT vleresimi FROM provimi WHERE id_lenda=? AND id_studenti=?";
	private final String DELETE_QUERY = "DELETE FROM provimi WHERE id_lenda=? AND id_studenti=?";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean insert(Provimi provimi) throws EntityExistException{
		@SuppressWarnings("rawtypes")
		Map m = new HashMap();
		m.put("lid", provimi.getId_lenda());
		m.put("sid", provimi.getId_studenti());
		m.put("vleresimi", provimi.getVleresimi());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try{
			return getNamedParameterJdbcTemplate().update(INSERT_QUERY, ps) != 0;
		}catch(Throwable t){
			throw new EntityExistException(t.getMessage()); 
		}
	}

	@Override
	public boolean update(Provimi provimi) {
		throw new UnsupportedOperationException("Operation not supported!");
	}

	@Override
	public Integer getVleresimi(Integer lendaID, Integer studentID) {
		return ((Provimi)getJdbcTemplate().queryForObject(GET_QEURY, new ProvimiRM(), lendaID, studentID)).getVleresimi();
	}

	@Override
	public boolean delete(Provimi provimi) {
		return getJdbcTemplate().update(DELETE_QUERY, provimi.getId_lenda(), provimi.getId_studenti()) != 0;
	}
	
}
