package me.genti.dao.interfaces;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Professor;
import me.genti.model.Provimi;
import me.genti.model.Student;

public interface ProvimiDAOInterface {
	
	/**
	 * Persiste a new {@code Provimi} into persistent data.
	 * @param provimi object to be persisted
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Provimi provimi)throws EntityExistException;
	
	/**
	 * Merge {@code this} {@link Provimi} object changes with its {@code Entity} in persistent data.
	 * @param provimi object with changes to be merged.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(Provimi provimi) throws EntityExistException;
	
	/**
	 * Remove {@link Provimi} from persistent data.
	 * @param provimi object to be removed from persisted data.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Provimi provimi);
	
	/**
	 * Get exam evaluation ({@link Provimi#getVleresimi()}) for a {@link Student} set by a {@link Professor}
	 * @param profesorID {@link Professor#getId()}
	 * @param studentID {@link Student#getId()}
	 * @return {@link Integer} representing student evaluation for a exam if found, otherwise {@code null}
	 */
	public Integer getVleresimi(Integer profesorID, Integer studentID);
}
