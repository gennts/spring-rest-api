package me.genti.dao.interfaces;

import java.util.List;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Detyra;
import me.genti.model.Lenda;

public interface DetyraDAOInterface {
	
	/**
	 * Insert a {@link Detyra} object in persistent database
	 * @param detyra object to be inserted in database
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException in case the current object trying to be inserted already exists in database.
	 */
	public boolean insert(Detyra detyra)throws EntityExistException;
	
	/**
	 * Updates a {@link Detyra} {@code Entity} contained in database data.
	 * @param detyra object containing changes 
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException in case the current object trying to be updated contains changes that 
	 * 			obliterate data integrity.
	 */
	public boolean update(Detyra detyra)throws EntityExistException;
	
	/**
	 * Delete a {@link Detyra} {@code Entity}
	 * @param detyra object to be deleted from persisted data.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Detyra detyra);
	
	/**
	 * Method returns a list of all {@link Detyra} objects.
	 * @param lendaID {@link Lenda#getId()}
	 * @return a {@link List} containing all {@link Detyra} objects found in persisted data.
	 */
	public List<Detyra> getDetyrat(Integer lendaID);
}
