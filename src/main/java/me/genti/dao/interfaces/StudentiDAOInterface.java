package me.genti.dao.interfaces;

import java.util.List;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Student;

public interface StudentiDAOInterface {
	
	/**
	 * Persist a new {@link Student} object into persistent data
	 * @param student object to be persisted.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Student student)throws EntityExistException;
	
	/**
	 * Merge a {@link Student} object containing changes with its {@code Entity} in persistent data
 	 * @param student object to be merged.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(Student student)throws EntityExistException;
	
	/**
	 * Remove a {@link Student} object from persistent data.
	 * @param student to be removed from persistent data.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Student student);
	
	/**
	 * Method retrieves all {@link Student}s found in persistent data
	 * @return {@link List} of {@link Student}s 
	 */
	public List<Student> getAllStudents();
}
