package me.genti.dao.interfaces;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Lenda;
import me.genti.model.Pjesmarrja;
import me.genti.model.Professor;
import me.genti.model.Student;

public interface PjesmarrjaDAOInterface {
	
	/**
	 * Persist a new {@link Pjesmarrja} into persistent data.
	 * @param pjesmarrja object to be persisted
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Pjesmarrja pjesmarrja) throws EntityExistException;
	
	/**
	 * Merge a {@link Pjesmarrja} with corresponding persistent {@code Entity} in persistend data.
	 * @param pjesmarrja object to be merged
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(Pjesmarrja pjesmarrja) throws EntityExistException;
	
	/**
	 * Remove a {@link Pjesmarrja} from persisted data.
	 * @param pjesmarrja object to be removed
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Pjesmarrja pjesmarrja);
	
	/**
	 * Method retrieves student attendance at a specified {@link Lenda} given by a {@link Professor}
	 * @param studentID {@link Student#getId()}
	 * @param professorID {@link Professor#getId()}
	 * @param lendaID {@link Lenda#getId()}
	 * @return {@link Integer} representing {@link Student} attendance for specified {@link Lenda} and {@link Professor} if
	 * 			result is found, otherwise return {@code null}
	 */
	public Integer getStudentAttendance(Integer studentID, Integer professorID, Integer lendaID);
}
