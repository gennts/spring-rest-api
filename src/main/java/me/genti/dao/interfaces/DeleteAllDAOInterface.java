package me.genti.dao.interfaces;

public interface DeleteAllDAOInterface {
	
	/**
	 * Delete every bit of data contained in this database
	 */
	public void deleteAll();
}
