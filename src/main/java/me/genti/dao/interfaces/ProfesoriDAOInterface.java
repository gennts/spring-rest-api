package me.genti.dao.interfaces;

import java.util.List;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Professor;

public interface ProfesoriDAOInterface {
	
	/**
	 * Persist a new {@link Professor} object in persistent data.
	 * @param profesor object to be persisted.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Professor profesor)throws EntityExistException;
	
	/**
	 * Merge {@code this} {@link Professor} object with its {@code Entity} in persistent data.
	 * @param profesor object to be merged.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(Professor profesor)throws EntityExistException;
	
	/**
	 * Remove {@link Professor} from persistent data.
	 * @param profesor object to be removed.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Professor profesor);
	
	/**
	 * Retrieve all {@link Professor}s found in persistent data.
	 * @return {@code List} of all {@link Professor}
	 */
	public List<Professor> getAllProfessors();
}
