package me.genti.dao.interfaces;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Lenda;
import me.genti.model.ProjektiGrupor;
import me.genti.model.Student;

public interface ProjektiGruporDAOInterface {
	
	/**
	 * Persist a new {@link ProjektiGrupor} to persistent data.
	 * @param pg object to be persisted.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(ProjektiGrupor pg)throws EntityExistException;
	
	/**
	 * Merge {@code this} {@link ProjektiGrupor} object with its persisted {@code Entity} data.
	 * @param pg object to be persisted
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(ProjektiGrupor pg)throws EntityExistException;
	
	/**
	 * Remove a {@link ProjektiGrupor} object from persistent data. 
	 * @param pg object to be removed.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(ProjektiGrupor pg);
	
	/**
	 * Retrieve the {@link ProjektiGrupor} evaluation of a {@link Student} in a specified {@link Lenda}
	 * @param studentID {@link Student#getId()}
	 * @param lendaID {@link Lenda#getId()}
	 * @return {@link Integer} representing {@link Student} evaluation if found, otherwise {@code null}
	 */
	public Integer getVleresimiProjektiGrupor(Integer studentID, Integer lendaID);
}
