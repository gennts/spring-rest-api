package me.genti.dao.interfaces;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Lenda;

public interface LendaDAOInterface {
	
	/**
	 * Persist a new {@link Lenda} object.
	 * @param lenda object to be persisted!
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Lenda lenda)throws EntityExistException;
	
	/**
	 * Merges {@code this} object with existing Entity in database
	 * @param lenda object to be merged
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean update(Lenda lenda)throws EntityExistException;
	
	/**
	 * Removes {@link Lenda} from persistent data.
	 * @param lenda object to be removed
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Lenda lenda);
}
