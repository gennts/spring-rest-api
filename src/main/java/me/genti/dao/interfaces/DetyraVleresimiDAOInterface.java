package me.genti.dao.interfaces;

import me.genti.exceptions.EntityExistException;
import me.genti.model.Detyra;
import me.genti.model.Student;
import me.genti.model.Vleresimi;

public interface DetyraVleresimiDAOInterface {
	
	/**
	 * Persists a new {@link Vleresimi} object into persisten data.
	 * @param vleresimi object to be persisted.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object obliterates persisted data integrity.
	 */
	public boolean insert(Vleresimi vleresimi) throws EntityExistException;
	
	/**
	 * Merge {@link Vleresimi} with a corresponding persistent Entity in database
	 * @param vleresimi object to be merge
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 * @throws EntityExistException if {@code this} object changes obliterates persisted data integrity.
	 */
	public boolean update(Vleresimi vleresimi) throws EntityExistException;
	
	/**
	 * Remove {@code this} object from persistent data.
	 * @param vleresimi object to be removed.
	 * @return {@link Boolean#TRUE} if transaction was successful otherwise {@link Boolean#FALSE}
	 */
	public boolean delete(Vleresimi vleresimi);
	
	/**
	 * Retrieve evaluation from persistent data based on keys provided on method paramaters.
	 * @param detyraID {@link Detyra#getId()}
	 * @param studentID {@link Student#getId()}
	 * @return {@link Integer} if result based on keys received is found, otherwise {@code null}
	 */
	public Integer getVleresimiDetyra(Integer detyraID, Integer studentID);
	
}
