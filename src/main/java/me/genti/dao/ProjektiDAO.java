package me.genti.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.ProjektiGruporDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.ProjektiGrupor;
import me.genti.rowmapper.ProjektiRM;

@Repository
public class ProjektiDAO extends BaseDAO implements ProjektiGruporDAOInterface{
	
	private final String INSERT_QUERY = "INSERT INTO projektigrupor VALUES(:gid, :sid, :lid, :vlersimi)";
	private final String GET_QUERY = "SELECT vleresimi FROM projektigrupor WHERE id_studenti=? AND id_lenda=?";
	private final String DELETE_QUERY = "DELETE FROM projektigrupor where id_grupi=?";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean insert(ProjektiGrupor pg)throws EntityExistException {
		@SuppressWarnings("rawtypes")
		Map m = new HashMap();
		m.put("gid", pg.getId_grupi());
		m.put("sid", pg.getId_studenti());
		m.put("lid", pg.getId_lenda());
		m.put("vlersimi", pg.getVleresimi());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try{
			return getNamedParameterJdbcTemplate().update(INSERT_QUERY, ps) != 0;
		}catch(Throwable t){
			throw new EntityExistException(t.getMessage());
		}
	}

	@Override
	public boolean update(ProjektiGrupor pg) {
		throw new UnsupportedOperationException("Operation not supported yet!");
	}

	@Override
	public Integer getVleresimiProjektiGrupor(Integer studentID, Integer lendaID) {
		return ((ProjektiGrupor)getJdbcTemplate().queryForObject(GET_QUERY, new ProjektiRM(), studentID, lendaID)).getVleresimi();
	}

	@Override
	public boolean delete(ProjektiGrupor pg) {
		return getJdbcTemplate().update(DELETE_QUERY, pg.getId_grupi()) != 0;
	}

}
