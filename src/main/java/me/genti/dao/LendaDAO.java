package me.genti.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.LendaDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Lenda;

@Repository
public class LendaDAO extends BaseDAO implements LendaDAOInterface{
	
	private final String INSERT_QUERY = "INSERT INTO lenda VALUES(:id, :emri, :pershkrimi, :oret, :detyrat)";
	private final String DELETE_QUERY = "DELETE FROM lenda WHERE id=?";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean insert(Lenda lenda)throws EntityExistException {
		@SuppressWarnings("rawtypes")Map m = new HashMap();
		m.put("id", lenda.getId());
		m.put("emri", lenda.getEmri());
		m.put("pershkrimi", lenda.getPershkrimi());
		m.put("oret", lenda.getOret());
		m.put("detyrat", lenda.getDetyrat());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try{
			return getNamedParameterJdbcTemplate().update(INSERT_QUERY, ps) != 0;
		}catch(Throwable t){
			throw new EntityExistException(t.getMessage());
		}
	}

	@Override
	public boolean update(Lenda lenda) {
		throw new UnsupportedOperationException("Operation not supported yet!");
	}

	@Override
	public boolean delete(Lenda lenda) {
		return getJdbcTemplate().update(DELETE_QUERY, lenda.getId()) != 0;
	}

}
