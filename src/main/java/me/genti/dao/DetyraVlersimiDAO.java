package me.genti.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.DetyraVleresimiDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Vleresimi;
import me.genti.rowmapper.VleresimiRM;

@Repository
public class DetyraVlersimiDAO extends BaseDAO implements DetyraVleresimiDAOInterface{
	
	private final String INSERT_QUERY = "INSERT INTO vleresimi VALUES(:did, :sid, :vler)";
	private final String GET_QUERY = "SELECT vleresimi FROM vleresimi WHERE id_detyra=? and id_studenti=?";
	private final String DELETE_QUERY = "DELETE FROM vleresimi WHERE id_detyra=? AND id_studenti=?";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean insert(Vleresimi vleresimi) throws EntityExistException{
		@SuppressWarnings("rawtypes")
		Map m = new HashMap();
		m.put("did", vleresimi.getId_detyra());
		m.put("sid", vleresimi.getId_studenti());
		m.put("vler", vleresimi.getVleresimi());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try{
			return getNamedParameterJdbcTemplate().update(INSERT_QUERY, ps) != 0;
		}catch(Throwable t){
			throw new EntityExistException(t.getMessage());
		}
	}

	@Override
	public boolean update(Vleresimi vleresimi) {
		throw new UnsupportedOperationException("Unsupported opperation");	
	}

	@Override
	public Integer getVleresimiDetyra(Integer detyraID, Integer studentID) {
		return ((Vleresimi)getJdbcTemplate().queryForObject(GET_QUERY, new VleresimiRM(), detyraID, studentID)).getVleresimi();
	}

	@Override
	public boolean delete(Vleresimi vleresimi) {
		return getJdbcTemplate().update(DELETE_QUERY, vleresimi.getId_detyra(), vleresimi.getId_studenti()) != 0;
	}

}
