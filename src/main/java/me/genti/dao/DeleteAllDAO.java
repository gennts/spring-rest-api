package me.genti.dao;

import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.DeleteAllDAOInterface;

@Repository
public class DeleteAllDAO extends BaseDAO implements DeleteAllDAOInterface{

	@Override
	public void deleteAll() {
		getJdbcTemplate().execute("delete from provimi");
		getJdbcTemplate().execute("delete from pjesmarrja");
		getJdbcTemplate().execute("delete from projektigrupor");
		getJdbcTemplate().execute("delete from vleresimi");
		getJdbcTemplate().execute("delete from detyra");
		getJdbcTemplate().execute("delete from lenda");
		getJdbcTemplate().execute("delete from studenti");
		getJdbcTemplate().execute("delete from profesori");
	}

}
