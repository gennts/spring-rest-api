package me.genti.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import me.genti.dao.interfaces.StudentiDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Student;
import me.genti.rowmapper.StudentRM;

@Repository
public class StudentiDAO extends BaseDAO implements StudentiDAOInterface {
	
	private final String INSERT_QUERY = "INSERT INTO studenti VALUES(:id, :emri,:mbiemri,:email,:nrtel)";
	private final String UPDATE_QUERY = "UPDATE studenti SET emri=:emri, mbiemri=:mbiemri, email=:email, nrtel=:nrtel WHERE id=:id";
	private final String GET_QUERY = "SELECT * FROM studenti";
	private final String DELETE_QUERY = "DELETE FROM studenti WEHRE id=?";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean insert(Student student) throws EntityExistException {
		@SuppressWarnings("rawtypes")
		Map m = new HashMap();
		m.put("id", student.getId());
		m.put("emri", student.getEmri());
		m.put("mbiemri", student.getMbiemri());
		m.put("email", student.getEmail());
		m.put("nrtel", student.getNrtel());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try {
			return getNamedParameterJdbcTemplate().update(INSERT_QUERY, ps) != 0;
		} catch (Throwable t) {
			if (t.getMessage()
					.contains("The statement was aborted because it would have caused a duplicate key value ")) {
				throw new EntityExistException("\"error\":{\n\t\"errortxt\": \"A student with that ID already exists!\"\n}");
			}return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean update(Student student)throws EntityExistException {
		@SuppressWarnings("rawtypes")
		Map m = new HashMap();
		m.put("id", student.getId());
		m.put("emri", student.getEmri());
		m.put("mbiemri", student.getMbiemri());
		m.put("email", student.getEmail());
		m.put("nrtel", student.getNrtel());
		SqlParameterSource ps = new MapSqlParameterSource(m);
		try {
			return getNamedParameterJdbcTemplate().update(UPDATE_QUERY, ps) != 0;
		} catch (Throwable t) {
			throw new EntityExistException(t.getMessage());
		}
	}

	@Override
	public List<Student> getAllStudents() {
		return getJdbcTemplate().query(GET_QUERY, new StudentRM());
	}
	
	@Override
	public boolean delete(Student student) {
		return getJdbcTemplate().update(DELETE_QUERY, student.getId()) != 0;
	}
}
