package me.genti.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.util.ResourceUtils;

public class PropertyUtil {
	
	private static final Logger LOG = Logger.getLogger(PropertyUtil.class);
	
	/**
	 * Load properties file from resources folder
	 */
	public static final void loadProperties(Properties props, String fileName) {
		try{
			props.load(new FileInputStream(ResourceUtils.getFile("classpath:"+fileName)));
		}catch(IOException e) {
			LOG.error(e, e);
		}
	}
}
