package me.genti.utils;

import java.util.Properties;

import me.genti.model.Error;

public class ErrorUtil {
	
	private static final Integer ENTITY_EXISTS_CODE,
							     UNDEFINED_ERROR_CODE,
							     CONSTRUCTED_ERROR = 300;
	
	private static final String ENTITY_EXISTS_ERROR_MSG,
								UNDEFINED_ERROR_MSG;
	
	private static final Error ENTITY_EXISTS_OBJECT,
							   UNDEFINE_ERROR_OBJECT;
	
	private static final String KEY_ERROR_ENTITY_EXISTS = "entity_exists_err",
							 	KEY_ERROR_UNDEFINED 	= "undefined_err",
							 	KEY_ERROR_ENTITY_EXISTS_CODENUMBER = "entity_exists_code",
							 	KEY_ERROR_UNDEFINED_CODENUMBER = "undefined_code";
	
	private static final String FILENAME = "error.properties";
	
	static {
		final Properties props = new Properties();
		PropertyUtil.loadProperties(props, FILENAME);
		
		ENTITY_EXISTS_ERROR_MSG = props.getProperty(KEY_ERROR_ENTITY_EXISTS);
		UNDEFINED_ERROR_MSG = props.getProperty(KEY_ERROR_UNDEFINED);
		
		ENTITY_EXISTS_CODE = Integer.parseInt(props.getProperty(KEY_ERROR_ENTITY_EXISTS_CODENUMBER));
		UNDEFINED_ERROR_CODE = Integer.parseInt(props.getProperty(KEY_ERROR_UNDEFINED_CODENUMBER));
		
		ENTITY_EXISTS_OBJECT = new Error(ENTITY_EXISTS_CODE, ENTITY_EXISTS_ERROR_MSG);
		UNDEFINE_ERROR_OBJECT = new Error(UNDEFINED_ERROR_CODE, UNDEFINED_ERROR_MSG);
	}
	
	public final static Error getEntityExistsError() {
		return ENTITY_EXISTS_OBJECT;
	}
	
	public final static Error getUndefinedError() {
		return UNDEFINE_ERROR_OBJECT;
	}
	
	public final static Error constructNewError(final String error_msg) {
		return new Error(CONSTRUCTED_ERROR, error_msg);
	}
}
