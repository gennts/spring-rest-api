package me.genti.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages={"me.genti"})
@EnableWebMvc
public class WebMVC extends WebMvcConfigurerAdapter{

	@Bean public ViewResolver getViewResolver(){
		return new InternalResourceViewResolver("/WEB-INF/views/", ".jsp");
	}
}
