package me.genti.configuration;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;

import org.apache.derby.drda.NetworkServerControl;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import me.genti.utils.PropertyUtil;

@Configuration
@ComponentScan(basePackages=("me.genti"))
public class Root {
	
	/**
	 * Default logger for {@link Root}
	 */
	private final Logger LOG = Logger.getLogger(this.getClass());
	
	/**
	 * Derby server controller; initialized on runtime
	 */
	private NetworkServerControl serverControl = null;
	
    /**
     * Datasource (connection pool) containing ready to use database connections
     */
	private DataSource dataSource = null;
	
	/**
	 * Derby Server running state
	 */
	private boolean derbyRunningState = false;
	
	/**
	 * Properties file containing necessary configuration for Derby DB
	 */
	private Properties databaseData = new Properties();
	
	/**
	 * Datasource data configuration
	 */
	private final String CONFIG_FILE = "configuration.properties",
			
						 USERNAME = "username",
						 PASSWORD = "password",
						 URL      = "url";
	
	/**
	 * Method used to retrieve data source of this application.
	 * @return {@link DataSource}
	 */
	@Bean public DataSource getDataSource(){
		if(dataSource == null) {
			PropertyUtil.loadProperties(databaseData, CONFIG_FILE);;
			DriverManagerDataSource dataSourceInitializer = new DriverManagerDataSource();
			dataSourceInitializer.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
			dataSourceInitializer.setUrl("jdbc:derby:"+databaseData.getProperty(URL));
			dataSourceInitializer.setUsername(databaseData.getProperty(USERNAME));
			dataSourceInitializer.setPassword(databaseData.getProperty(PASSWORD));
			dataSource = dataSourceInitializer;
		}return dataSource;
	}
	
	/**
	 * Start derby server on load time
	 */
	@PostConstruct public void startDerbyServer(){	
		if(derbyIsRunning())return;
        System.setProperty("derby.drda.startNetworkServer", "true");
        try {
            serverControl = new NetworkServerControl(InetAddress.getByName(getIpv4()), 1527);
            serverControl.start(new PrintWriter(System.out, true));
            derbyRunningState = true;
        } catch (Exception ex) {
            LOG.error(ex, ex);
        }
    }
	
	/**
	 * stop derby server when application terminates
	 */
	@PreDestroy public void stopDerby(){
		if (serverControl != null)
			try {
				serverControl.shutdown();
				derbyRunningState = false;
			} catch (Exception e) {
				LOG.error(e, e);
			}
	}
	
	/**
	 * Checks if derby server is running 
	 * @return {@link Boolean#TRUE} if derby is running otherwise {@link Boolean#FALSE}
	 */
	private boolean derbyIsRunning() {
		if(derbyRunningState)return true;
		try(final Socket s = new Socket()) {
			s.connect(new InetSocketAddress(getIpv4(), 1527));
		}catch(IOException e) {
			return false;
		}return true;
	}
	
	/**
	 * Method returns current machine IPv4
	 * @return String representation of machine's IPv4
	 */
	private final String getIpv4() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException ex) {
			LOG.error(ex, ex);
		}return null;
	}
}
