package me.genti.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import me.genti.dao.interfaces.DeleteAllDAOInterface;
import me.genti.exceptions.EntityExistException;
import me.genti.model.Detyra;
import me.genti.model.Lenda;
import me.genti.model.Pjesmarrja;
import me.genti.model.Professor;
import me.genti.model.ProjektiGrupor;
import me.genti.model.Provimi;
import me.genti.model.Student;
import me.genti.model.Vleresimi;
import me.genti.services.interfaces.LendaServiceInterface;
import me.genti.services.interfaces.ProfesoriServiceInterface;
import me.genti.services.interfaces.StudentiServiceInterface;
import me.genti.utils.ErrorUtil;

@RestController
@RequestMapping("/api")
public class Rest {
	
	@Autowired private LendaServiceInterface lendaService;
	@Autowired private ProfesoriServiceInterface profesoriService;
	@Autowired private StudentiServiceInterface studentiService;
	
	@Autowired private DeleteAllDAOInterface deleteAllDAO;
	
	private final Gson GSON = new Gson();
	
	@RequestMapping(value = "/student/insert", method = RequestMethod.POST)
	@ResponseBody public String insertStudent(@RequestParam("id") Integer id, @RequestParam("emri") String emri ,@RequestParam("mbiemri") String mbiemri, @RequestParam("email") String email,@RequestParam("nrtel") String nrtel){
		try{
			final Student st = new Student(id, emri, mbiemri, email, nrtel);
			if(studentiService.insert(st))
				return GSON.toJson(st);
		}catch(EntityExistException t){
			return GSON.toJson(ErrorUtil.getEntityExistsError());
		}return GSON.toJson(ErrorUtil.getUndefinedError());
	}

	@RequestMapping(value = "/student/all", method = RequestMethod.GET)
	@ResponseBody public String getAllStudents(){
		return GSON.toJson(studentiService.getAllStudents());
	}
	
	@RequestMapping(value = "/lenda/insert", method = RequestMethod.POST)
	@ResponseBody public String insetLenda(@RequestParam("id")Integer id, @RequestParam("emri")String emri, @RequestParam("pershkrimi")String pershkrimi, @RequestParam("oret")Integer oret, @RequestParam("detyrat")Integer detyrat){
		try{
			final Lenda lenda = new Lenda(id, emri, pershkrimi, oret, detyrat);
			if(lendaService.insert(lenda))
				return GSON.toJson(lenda);
		}catch(EntityExistException e){
			return GSON.toJson(ErrorUtil.getEntityExistsError());
		} return GSON.toJson(ErrorUtil.getUndefinedError());
	}
	
	@RequestMapping(value = "/detyra/insert", method = RequestMethod.POST)
	@ResponseBody public String insertDetyra(@RequestParam("id")Integer id, @RequestParam("lid")Integer lid, @RequestParam("titulli")String titulli){
		try{
			final Detyra detyra = new Detyra(id, lid, titulli);
			if(lendaService.insert(detyra))
				return GSON.toJson(detyra);
		}catch(EntityExistException e){
			return GSON.toJson(ErrorUtil.getEntityExistsError());
		} return GSON.toJson(ErrorUtil.getUndefinedError());
	}
	
	@RequestMapping(value = "/profesor/insert", method = RequestMethod.POST)
	@ResponseBody public String insertProfesor(@RequestParam("id") Integer id, @RequestParam("emri") String emri ,@RequestParam("mbiemri") String mbiemri, @RequestParam("email") String email,@RequestParam("nrtel") String nrtel){
		try{
			final Professor professor = new Professor(id, emri, mbiemri, email, nrtel);
			if(profesoriService.insert(professor))
				return GSON.toJson(professor);
		}catch(EntityExistException e){
			return GSON.toJson(ErrorUtil.getEntityExistsError());
		} return GSON.toJson(ErrorUtil.getUndefinedError());
	}
	
	
	@RequestMapping(value = "/profesor/all", method = RequestMethod.GET)
	@ResponseBody public String getAllProfessors(){
		return GSON.toJson(profesoriService.getAllProfessors());
	}
	
	@RequestMapping(value = "/pjesmarrja/insert", method = RequestMethod.POST)
	@ResponseBody public String insertPjesmarrja(@RequestParam("ids")Integer ids, @RequestParam("idl")Integer idl, @RequestParam("idp")Integer idp, @RequestParam("pjesmarrja")Integer pjesmarrja){
		try{
			final Pjesmarrja pjesmarrj = new Pjesmarrja(ids, idl, idp, pjesmarrja);
			if(profesoriService.insert(pjesmarrj))
				return GSON.toJson(pjesmarrj);
		}catch(EntityExistException e){
			return GSON.toJson(ErrorUtil.constructNewError(e.getMessage()));
		} return GSON.toJson(ErrorUtil.getUndefinedError());
	}
	
	@RequestMapping(value = "/pjesmarrja/get", method = RequestMethod.POST)
	@ResponseBody public String getStudentAttendance(@RequestParam("sid")Integer sid, @RequestParam("pid")Integer pid, @RequestParam("lid")Integer lid){
		return "{\n\t\"pjesmarrja\": \""+profesoriService.getStudentAttendance(sid, pid, lid)+"\" \n}";
	}
	
	@RequestMapping(value = "/projekti/insert", method = RequestMethod.POST)
	@ResponseBody public String insertProjekt(@RequestParam("gid")Integer gid, @RequestParam("sid")Integer sid, @RequestParam("lid")Integer lid, @RequestParam("vlersimi")Integer vlersimi){
		try{
			final ProjektiGrupor projektiGrupor = new ProjektiGrupor(gid, sid, lid, vlersimi);
			if(profesoriService.insert(projektiGrupor))
				return GSON.toJson(projektiGrupor);
		}catch(EntityExistException e){
			return GSON.toJson(ErrorUtil.getEntityExistsError());
		} return GSON.toJson(ErrorUtil.getUndefinedError());
	}
	
	@RequestMapping(value = "/projekti/get", method = RequestMethod.POST)
	@ResponseBody public String getVlersimiProjektit(@RequestParam("sid")Integer sid, @RequestParam("lid")Integer lid){
		return "{\n\t\"vlersimigrupor\" : \""+profesoriService.getVleresimiProjektiGrupor(sid, lid)+"\"\n}";
	}
	
	@RequestMapping(value = "/provimi/insert", method = RequestMethod.POST)
	@ResponseBody public String insertProvim(@RequestParam("lid")Integer lid, @RequestParam("sid")Integer sid, @RequestParam("vl")Integer vl){
		try{
			final Provimi provimi = new Provimi(sid, lid, vl);
			if(profesoriService.insert(provimi))
				return GSON.toJson(provimi);
		}catch(EntityExistException e){
			return GSON.toJson(ErrorUtil.getEntityExistsError());
		} return GSON.toJson(ErrorUtil.getUndefinedError());
	}
	
	@RequestMapping(value = "/provimi/get", method = RequestMethod.POST)
	@ResponseBody public String getVlersimiProvimit(@RequestParam("pid")Integer pid, @RequestParam("sid")Integer sid){
		return "{\n\t\"provimi\" : \""+profesoriService.getProvimiVleresimi(pid, sid)+"\"\n}";
	}
	
	@RequestMapping(value = "/detyra/vlersimi/insert", method = RequestMethod.POST)
	@ResponseBody public String insertDetyraVleresimi(@RequestParam("did")Integer did, @RequestParam("sid")Integer sid, @RequestParam("vl")Integer vl){
		try{
			final Vleresimi vleresimi = new Vleresimi(did, sid, vl);
			if(profesoriService.insert(vleresimi))
				return GSON.toJson(vleresimi);
		}catch(EntityExistException e){
			return GSON.toJson(ErrorUtil.getEntityExistsError());
		} return GSON.toJson(ErrorUtil.getUndefinedError());
	}
	
	@RequestMapping(value = "/detyra/get", method = RequestMethod.POST)
	@ResponseBody public String getVlersimiDetyres(@RequestParam("did")Integer did, @RequestParam("sid")Integer sid){
		return "{\n\t\"vlersimi_detyres\" : \""+profesoriService.getVleresimiDetyra(did, sid)+"\"\n}";
	}
	
	@RequestMapping(value = "/kalkulo", method = RequestMethod.POST)
	@ResponseBody public String kalkuloNoten(@RequestParam("sid")Integer studentID, @RequestParam("pid")Integer profesorID, @RequestParam("lid") Integer lendaID){
		return "{\n\tnota_finale\" : \""+profesoriService.kalkuloNoten(studentID, profesorID, lendaID)+"\"\n}";
	}
	
	@RequestMapping("/delete/all")
	public void deleteAll() {
		deleteAllDAO.deleteAll();
	}
}
