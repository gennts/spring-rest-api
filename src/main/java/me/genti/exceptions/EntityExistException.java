package me.genti.exceptions;

public class EntityExistException extends Exception{

	private static final long serialVersionUID = -9223323868839408888L;

	public EntityExistException(String msg) {
		super(msg);
	}
}
