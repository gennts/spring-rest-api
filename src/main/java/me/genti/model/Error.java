package me.genti.model;

public class Error {
	
	private final int error_id;
	private final String error_msg;
	
	public Error(int error_id, String error_msg) {
		this.error_id = error_id;
		this.error_msg = error_msg;
	}
	
	public int getError_id() {
		return error_id;
	}
	
	public String getError_msg() {
		return error_msg;
	}
	
}
