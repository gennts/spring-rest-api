package me.genti.model;

public class Provimi {
	private Integer id_studenti;
	private Integer id_lenda;
	private Integer vleresimi;
	
	public Provimi() {}
	
	
	public Provimi(Integer id_studenti, Integer id_lenda, Integer vleresimi) {
		this.id_studenti = id_studenti;
		this.id_lenda = id_lenda;
		this.vleresimi = vleresimi;
	}


	public Integer getId_studenti() {
		return id_studenti;
	}
	public void setId_studenti(Integer id_studenti) {
		this.id_studenti = id_studenti;
	}
	public Integer getId_lenda() {
		return id_lenda;
	}
	public void setId_lenda(Integer id_lenda) {
		this.id_lenda = id_lenda;
	}
	public Integer getVleresimi() {
		return vleresimi;
	}
	public void setVleresimi(Integer vleresimi) {
		this.vleresimi = vleresimi;
	}



	
}
