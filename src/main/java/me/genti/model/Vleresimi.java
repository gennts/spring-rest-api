package me.genti.model;

public class Vleresimi {

	private Integer id_detyra;
	private Integer id_studenti;
	private Integer vleresimi;
	
	public Vleresimi() {}
	
	
	public Vleresimi(Integer id_detyra, Integer id_studenti, Integer vleresimi) {
		this.id_detyra = id_detyra;
		this.id_studenti = id_studenti;
		this.vleresimi = vleresimi;
	}


	public Integer getId_detyra() {
		return id_detyra;
	}
	public void setId_detyra(Integer id_detyra) {
		this.id_detyra = id_detyra;
	}
	public Integer getId_studenti() {
		return id_studenti;
	}
	public void setId_studenti(Integer id_studenti) {
		this.id_studenti = id_studenti;
	}
	public Integer getVleresimi() {
		return vleresimi;
	}
	public void setVleresimi(Integer vleresimi) {
		this.vleresimi = vleresimi;
	}

	
}
