package me.genti.model;

public class Student {
	
	private Integer id;
	private String emri;
	private String mbiemri;
	private String email;
	private String nrtel;
	
	public Student() {}
	
	
	public Student(Integer id, String emri, String mbiemri, String email, String nrtel) {
		this.id = id;
		this.emri = emri;
		this.mbiemri = mbiemri;
		this.email = email;
		this.nrtel = nrtel;
	}


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmri() {
		return emri;
	}
	public void setEmri(String emri) {
		this.emri = emri;
	}
	public String getMbiemri() {
		return mbiemri;
	}
	public void setMbiemri(String mbiemri) {
		this.mbiemri = mbiemri;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNrtel() {
		return nrtel;
	}
	public void setNrtel(String nrtel) {
		this.nrtel = nrtel;
	}
	
	
}
