package me.genti.model;

public class Pjesmarrja {
	
	private Integer id_studenti;
	private Integer id_lenda;
	private Integer id_profesori;
	private Integer pjesmarrja;
	
	public Pjesmarrja() {}
	
	
	public Pjesmarrja(Integer id_studenti, Integer id_lenda, Integer id_profesori, Integer pjesmarrja) {
		this.id_studenti = id_studenti;
		this.id_lenda = id_lenda;
		this.id_profesori = id_profesori;
		this.pjesmarrja = pjesmarrja;
	}


	public Integer getId_studenti() {
		return id_studenti;
	}
	public void setId_studenti(Integer id_studenti) {
		this.id_studenti = id_studenti;
	}
	public Integer getId_lenda() {
		return id_lenda;
	}
	public void setId_lenda(Integer id_lenda) {
		this.id_lenda = id_lenda;
	}
	public Integer getId_profesori() {
		return id_profesori;
	}
	public void setId_profesori(Integer id_profesori) {
		this.id_profesori = id_profesori;
	}
	public Integer getPjesmarrja() {
		return pjesmarrja;
	}
	public void setPjesmarrja(Integer pjesmarrja) {
		this.pjesmarrja = pjesmarrja;
	}


	
}
