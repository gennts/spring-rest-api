package me.genti.model;

public class Lenda {
	
	private Integer id;
	private String emri;
	private String pershkrimi;
	private Integer oret;
	private Integer detyrat;
	
	public Lenda() {}
	
	
	public Lenda(Integer id, String emri, String pershkrimi, Integer oret, Integer detyrat) {
		this.id = id;
		this.emri = emri;
		this.pershkrimi = pershkrimi;
		this.oret = oret;
		this.detyrat = detyrat;
	}


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmri() {
		return emri;
	}
	public void setEmri(String emri) {
		this.emri = emri;
	}
	public String getPershkrimi() {
		return pershkrimi;
	}
	public void setPershkrimi(String pershkrimi) {
		this.pershkrimi = pershkrimi;
	}
	public Integer getOret() {
		return oret;
	}
	public void setOret(Integer oret) {
		this.oret = oret;
	}
	public Integer getDetyrat() {
		return detyrat;
	}
	public void setDetyrat(Integer detyrat) {
		this.detyrat = detyrat;
	}
	
}
