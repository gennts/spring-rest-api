package me.genti.model;

public class ProjektiGrupor {
	
	private Integer id_grupi;
	private Integer id_studenti;
	private Integer id_lenda;
	private Integer vleresimi;
	
	public ProjektiGrupor() {}
	
	
	public ProjektiGrupor(Integer id_grupi, Integer id_studenti, Integer id_lenda, Integer vleresimi) {
		this.id_grupi = id_grupi;
		this.id_studenti = id_studenti;
		this.id_lenda = id_lenda;
		this.vleresimi = vleresimi;
	}


	public Integer getId_grupi() {
		return id_grupi;
	}
	public void setId_grupi(Integer id_grupi) {
		this.id_grupi = id_grupi;
	}
	public Integer getId_studenti() {
		return id_studenti;
	}
	public void setId_studenti(Integer id_studenti) {
		this.id_studenti = id_studenti;
	}
	public Integer getId_lenda() {
		return id_lenda;
	}
	public void setId_lenda(Integer id_lenda) {
		this.id_lenda = id_lenda;
	}
	public Integer getVleresimi() {
		return vleresimi;
	}
	public void setVleresimi(Integer vleresimi) {
		this.vleresimi = vleresimi;
	}



	
}
