package me.genti.model;

public class Detyra {
	
	private Integer id;
	private Integer id_lenda;
	private String titulli;
	
	public Detyra() {}
	
	public Detyra(Integer id, Integer id_lenda, String titulli) {
		this.id = id;
		this.id_lenda = id_lenda;
		this.titulli = titulli;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId_lenda() {
		return id_lenda;
	}
	public void setId_lenda(Integer id_lenda) {
		this.id_lenda = id_lenda;
	}
	public String getTitulli() {
		return titulli;
	}
	public void setTitulli(String titulli) {
		this.titulli = titulli;
	}
	
}
