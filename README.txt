A REST api developed with Spring Framework

Requirements:
	Java 1.8
	Tomcat 9

BEFORE DEPLOY SET DB PATH ON 'configuration.properties' url key : C:\Users\user\Desktop\kalkulimiNotes\database


					AVAILABLE HTTP REQUESTS (Use POSTMAN collection provided with this project for a complete example)
Student Services
	POST	http://localhost:8080/kalkulimiNotes/api/student/insert
	GET		http://localhost:8080/kalkulimiNotes/api/student/all
Lenda Services
	POST    http://localhost:8080/kalkulimiNotes/api/lenda/insert
	POST 	http://localhost:8080/kalkulimiNotes/api/detyra/insert

Profesor Services
	POST	http://localhost:8080/kalkulimiNotes/api/profesor/insert
	GET		http://localhost:8080/kalkulimiNotes/api/profesor/all
  Pjesmarrja
	POST 	http://localhost:8080/kalkulimiNotes/api/pjesmarrja/insert
	GET 	http://localhost:8080/kalkulimiNotes/api/pjesmarrja/get
  Projekti
	POST	http://localhost:8080/kalkulimiNotes/api/projekti/insert
	POST	http://localhost:8080/kalkulimiNotes/api/projekti/get
  Provimi
	POST	http://localhost:8080/kalkulimiNotes/api/provimi/insert
	POST	http://localhost:8080/kalkulimiNotes/api/provimi/get
  Detyra Vleresimi
	POST	http://localhost:8080/kalkulimiNotes/api/detyra/vlersimi/insert
	POST	http://localhost:8080/kalkulimiNotes/api/detyra/get
  Kalkulo
	POST 	http://localhost:8080/kalkulimiNotes/api/kalkulo
